package sopra.vol.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sopra.vol.Ville;
import sopra.vol.dao.IVilleDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class TestUltimate {

	@Autowired
	private IVilleDao villeDao;

	@Test
	public void aeroport() {

		int startSize = villeDao.findAll().size();

		Ville bordeaux = new Ville();
		bordeaux.setNom("Bordeaux");
		bordeaux = villeDao.save(bordeaux);

		Assert.assertEquals("Bordeaux", bordeaux.getNom());

		bordeaux.setNom("Merignac");
		bordeaux = villeDao.save(bordeaux);
		
		Assert.assertEquals("Merignac", bordeaux.getNom());
		
		bordeaux = villeDao.findByNom("Merignac");
		
		Assert.assertNotNull(bordeaux);	
		
		int middleSize = villeDao.findAll().size();
		Assert.assertEquals(1, middleSize - startSize);

		villeDao.delete(bordeaux);

		int endSize = villeDao.findAll().size();

		Assert.assertEquals(0, endSize - startSize);

	}

}
