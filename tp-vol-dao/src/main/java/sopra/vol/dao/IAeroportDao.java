package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Aeroport;

public interface IAeroportDao extends JpaRepository<Aeroport, String>{

}
