package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Ville;

public interface IVilleDao extends JpaRepository<Ville, Long> {
	Ville findByNom (String nom);
}
