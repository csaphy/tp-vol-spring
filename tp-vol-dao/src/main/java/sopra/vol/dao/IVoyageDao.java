package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Voyage;

public interface IVoyageDao extends JpaRepository<Voyage, Long> {
}
