package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Passager;

public interface IPassagerDao extends JpaRepository<Passager, Long> {
	Passager findByNom (String nom);
}
