package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sopra.vol.Compagnie;

public interface ICompagnieDao extends JpaRepository<Compagnie, Long>{
	
	@Query("from Compagnie where nomCompagnie = :monNomCompagnie")
	Compagnie findByNom (@Param("monNomCompagnie")String nom);
}
