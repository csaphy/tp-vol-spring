package sopra.vol.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.ClientPro;

public interface IClientDao extends JpaRepository<Client, Long>{
	
	@Query ("from ClientParticulier")
	List<ClientParticulier> findAllClientParticulier();
	
	@Query ("from ClientPro")
	List<ClientPro> findAllClientPro();
	
	@Query ("from Client where nom = :monNom")
	Client findByNom (@Param("monNom") String nom);
}
