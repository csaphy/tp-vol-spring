package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Vol;

public interface IVolDao extends JpaRepository<Vol, Long> {
}
