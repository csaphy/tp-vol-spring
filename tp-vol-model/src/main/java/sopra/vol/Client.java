package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_client")
public abstract class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String mail;
	private String telephone;
	@Enumerated(EnumType.STRING)
	private MoyenPaiement moyenPaiement;
	@Embedded
	private Adresse principale;
	@Embedded
	@AttributeOverrides({
		@AttributeOverride (name = "voie", column = @Column(name = "voie_f")),
		@AttributeOverride (name = "complement", column = @Column(name = "complement_f")),
		@AttributeOverride (name = "codePostal", column = @Column(name = "code_postal_f")),
		@AttributeOverride (name = "ville", column = @Column(name= "ville_f")),
		@AttributeOverride (name = "pays", column = @Column(name= "pays_f"))	
	})
	private Adresse facturation;

	@OneToMany(mappedBy = "client")
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Client() {
		super();
	}

	public Client(Long id, String mail, String telephone, MoyenPaiement moyenPaiement) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.moyenPaiement = moyenPaiement;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public MoyenPaiement getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	public Adresse getFacturation() {
		return facturation;
	}

	public void setFacturation(Adresse facturation) {
		this.facturation = facturation;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

}
